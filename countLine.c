#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <ctype.h>

void printdir(char *dir, int depth, char *file);
static long line = 0;
static long files = 0;
void count(char *filename, int depth);

int main(int argc, char *argv[])
{
    long int implementation = 0, implementationFile = 0;
    printdir("./Anubis", 0, "c");
    implementation = line;
    implementationFile = files;
    files = 0;
    line = 0;
#if 1
    printdir("./Anubis", 0, "h");
#endif
    printf("\nImplementation File: %ld, Header File: %ld, Header: %ld, Implementation: %ld, Total: %ld\n",
           implementationFile ,files, line, implementation, line+implementation);
	exit(0);
}//end main

char *get_filename_ext(const char* filename) {
    char *p_ext = NULL;
    
    p_ext = strrchr(filename, '.');
    if (p_ext) p_ext++;
    
    return p_ext;
}

void count(char *filename, int depth) {
    FILE *fp = fopen(filename, "r");
    if(!fp)
        return;
    char buffer[512];
    if(fp) {
        int startComment = 0;
        while(fgets(buffer, sizeof(buffer), fp)) {
            char *temp = buffer;
            while(temp != NULL && !isgraph(*temp))
                temp++;
            if(*temp == '\r' || *temp == '\n' || *temp == ' ' || *temp == '\t' || *temp == 0 || strlen(temp) == 0)
                continue;
            //略過單行註解
            if(temp && (*temp == '/' && *(temp+1) == '/')) {
                if(strstr(buffer, "Created"))
                    continue;
                if(strstr(buffer, "Copyright"))
                    continue;
                //if(strcmp(buffer, "//"))
                  //  continue;
                //if(strstr(buffer, "\n"))
                  //  continue;
                //printf("skip |%s|", buffer);
                continue;
            }
            //略過多行註解
            if(!startComment && (temp = strstr(buffer, "/*"))) {
                char *temp2 = buffer;
                while(temp2 != temp && temp2) {
                    if(temp2 && isgraph(*temp2))
                        break;
                    temp2++;
                }
                if(*temp2 && (temp2 == temp)) {
                    startComment = 1;
                }
                else {
                    //printf("%s", buffer); //code with comment
                }
            }
            if(startComment && strstr(buffer, "*/")) {
                //printf("%s", buffer);
                startComment = 0;
            }
            if(startComment) {
                //printf("%s", buffer);
                continue;
            }
            
            line++;
        }
    }
    printf("%*s%s\n", depth, "", filename);
    files++;
    fclose(fp);
}

void printdir(char *dir, int depth, char *file)
{
	DIR *dp; //目錄串流
	struct dirent *entry; //紀錄目錄內檔案
	struct stat statbuf;

	if( (dp = opendir(dir)) == NULL ) //開啓目錄串流
	{
		fprintf( stderr, "cannot open directory : %s\n", dir );
		return;
	}//end if

	chdir(dir); //切換目錄

	while((entry = readdir(dp)) != NULL ) //讀目錄下的檔案
	{
		lstat(entry->d_name, &statbuf); //回覆檔案狀態並寫入statbuf

		if(S_ISDIR(statbuf.st_mode)) { //S_ISDIR為巨集, 測是使否為目錄
			if(strcmp(".", entry->d_name) == 0 || strcmp("..", entry->d_name) == 0) //忽略.和..目錄
				continue;//深度, 是目錄多輸入/標記為目錄
            //skip pods folder
            
            if(!strcmp(entry->d_name, "win32"))
                continue;
            else if(!strcmp(entry->d_name, "missing"))
                continue;
            
            printf("%*s%s/\n", depth,"", entry->d_name); //depth記錄
			printdir(entry->d_name, depth+4, file); //遞迴呼叫
		}//end if
        else {
            char *exname = get_filename_ext(entry->d_name);
            if(!exname)
                continue;
            if(!strcmp(exname, file)) {
                
                if(!strcmp(entry->d_name, "countLine.c"))
                    continue;
                else if(!strcmp(entry->d_name, "json.c"))
                    continue;
                else if(!strcmp(entry->d_name, "json.h"))
                    continue;
                else if(!strcmp(entry->d_name, "config.h"))
                    continue;
                 
                count(entry->d_name, depth);
                //沒有/標記, 表示為普通檔案
            }
        }
	}//end while
	chdir(".."); //切換上一層處理下一個目錄
	closedir(dp); //關閉目前呼叫的目錄串流, 確保不會開太多目錄串流
}//end printdir
