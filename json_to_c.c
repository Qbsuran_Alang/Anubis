#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    
    if(argc != 2) {
        printf("%s file\n", argv[0]);
        return 1;
    }
    
    FILE *fp = fopen(argv[1], "r");
    char buffer[1024];
    int first = 1;
    int stack = 0;
    if(!fp) {
        perror("fopen()");
        return 1;
    }//end if
    
    while(!feof(fp)) {
        memset(buffer, 0, sizeof(buffer));
        fgets(buffer, sizeof(buffer), fp);
        
        printf("\"");
        
        if(first) {
            first = 0;
            printf("\\n");
        }
        
        for(int i = 0 ; i < strlen(buffer) ; i++) {
            if(!strncmp(buffer + i, "    ", 4)) {
                printf("\\t");
                i += 3;
                continue;
            }//end if
            if(buffer[i] == '\n') {
                break;
            }
            if(buffer[i] == '\"') {
                printf("\\\"");
                continue;
            }
            printf("%c", buffer[i]);
            if(buffer[i] == '[')
                stack++;
            if(buffer[i] == ']')
                stack--;
         }//end for
        
        if(stack == 0)
            printf("\"\n");
        else
            printf("\\n\"\n");
    }//end while
    
    fclose(fp);
    return 0;
}//end main