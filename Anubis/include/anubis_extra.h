//
//  anubis_extra.h
//  Anubis
//
//  Created by TUTU on 2016/4/12.
//
//
//        _____   _   _   _____   _   _
//       |_   _| | | | | |_   _| | | | |
//         | |   | | | |   | |   | | | |
//         | |   | |_| |   | |   | |_| |
//         |_|    \___/    |_|    \___/
//
//
//                               _       _
//       /\                     | |     (_)
//      /  \     _ __    _   _  | |__    _   ___
//     / /\ \   | '_ \  | | | | | '_ \  | | / __|
//    / ____ \  | | | | | |_| | | |_) | | | \__ \
//   /_/    \_\ |_| |_|  \__,_| |_.__/  |_| |___/


#ifndef anubis_extra_h
#define anubis_extra_h

void anubis_wait_microsecond(u_int32_t msec);
void anubis_fragment_offset(int data_len, int mtu, int ip_hl);
void anubis_list_devices(char *device);

#endif /* anubis_extra_h */
